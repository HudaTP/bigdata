from Crypto.Cipher import DES3
import base64

#  mode ECB , fill mode PKCS5Padding
BS = DES3.block_size
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s : s[0:-ord(s[-1])]

'''
     DES3 encryption
     Text string to be encrypted
     Key key, using appsecret
'''
def encrypt(text, key):
    text = pad(text)
    cipher = DES3.new(key,DES3.MODE_ECB)
    m = cipher.encrypt(text)
    m = base64.b64encode(m)
    return m.decode('utf-8')

'''
     DES3 decryption
     Decrypted_text decrypts the string
     Key key, using appsecret
'''
def decrypt(decrypted_text, key):
    text = base64.b64decode(decrypted_text)
    cipher = DES3.new(key, DES3.MODE_ECB)
    s = cipher.decrypt(text)
    s = unpad(s)
    return s.decode('utf-8')


if __name__ == '__main__':
    # print(encrypt('Hello','1234567887654321'))
    # print(decrypt('qO8nDeYzqTs=','1234567887654321'))
    #key
    #1)1234567887654321,e329232ea6d0d731,1234567887654322,1234567887654323,1234567887654324,1234567887654325,1234567887654326,1234567887654327,1234567887654328,1234567887654329


    import base64

    with open("D://mes_bigdata//MYHEALTH//mhealth//mhealth/static/a1.jpg", "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())

    print(encoded_string)
    aa=encrypt(encoded_string,'1234567887654329')
    print(type(aa))
    print("hlw")
    ##########encrypted save file


    import io

    encoding = 'utf-16le'

    with io.open("D://mes_bigdata//MYHEALTH//mhealth//mhealth/static/t1.txt", 'w', encoding=encoding) as f:
        f.write(aa)

    data = open("D://mes_bigdata//MYHEALTH//mhealth//mhealth/static/t1.txt").read()
    print("hhh")

    ##########decryption
    bb = decrypt(data, '1234567887654329')
    imgdata = base64.b64decode(bb)
    filename = 'D://mes_bigdata//MYHEALTH//mhealth//mhealth/static/a2.jpg'  # I assume you have a way of picking unique filenames
    with open(filename, 'wb') as f:
        f.write(imgdata)




