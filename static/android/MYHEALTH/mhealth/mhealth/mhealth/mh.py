from flask import Flask, render_template, request, session
from connection import conn
import datetime
import random
import qrcode

app = Flask(__name__)
app.secret_key = "keyyyy"
@app.route('/')
def ll():
    return render_template("login.html")

@app.route("/addpharmacy")
def addpharmacy():
    c = conn()
    sel = "select hid,name from addhospital"
    print(sel)
    res = c.selectall(sel)

    return render_template("admin_addPharmacy.html",res=res)

@app.route("/adm_view_emer")
def adm_view_emer():
    c = conn()
    sel = "select patient_entry.name,patient_entry.phno,alert.lati,alert.lonchi from alert,patient_entry where alert.uid=patient_entry.Pid"
    print(sel)
    res = c.selectall(sel)

    return render_template("admin_emergy_viw.html",res=res)




@app.route("/addambulance")
def addambulance():
    c = conn()
    sel = "select hid,name from addhospital"
    print(sel)
    res = c.selectall(sel)

    return render_template("admin_addambulance.html",res=res)


@app.route("/addlab")
def addalab():
    c = conn()
    sel = "select hid,name from addhospital"
    print(sel)
    res = c.selectall(sel)

    return render_template("admin_addLab.html",res=res)

@app.route("/addLab_post", methods=['post'])
def addlabmcy_post():
    hid = request.form['sss']
    cno = request.form["txt_cno"]
    email = request.form["txt_em"]
    pwd = request.form["txt_pwd"]
    c = conn()
    ins = "insert into lab1(hid,cno,email)values('"+hid+"','"+cno+"','"+email+"')"
    print(ins)
    c.nonreturn(ins)

    ins2 = "insert into userlogin values('"+email+"','"+pwd+"','lab')"
    print(ins2)
    c.nonreturn(ins2)

    return render_template("ADMINHOME.html")



@app.route("/addpharmcy_post", methods=['post'])
def addpharmcy_post():
    hid = request.form['sss']
    cno = request.form["txt_cno"]
    email = request.form["txt_em"]
    pwd = request.form["txt_pwd"]
    c = conn()
    ins = "insert into phar(hid,cno,email)values('"+hid+"','"+cno+"','"+email+"')"
    print(ins)
    c.nonreturn(ins)

    ins2 = "insert into userlogin values('"+email+"','"+pwd+"','phar')"
    print(ins2)
    c.nonreturn(ins2)

    return render_template("ADMINHOME.html")



@app.route("/addambulance_post", methods=['post'])
def addambulance_post():
    name = request.form['txtname']
    vehicleno = request.form["txtvehicleno"]
    place = request.form["Place"]
    post = request.form["Post"]
    pincode = request.form["Pincode"]
    Engineno = request.form["txtengineno"]
    phnno = request.form["txtphno"]
    uname = request.form["uname"]
    pwd = request.form["pwd"]
    hid2=request.form["sss"]
    discription = request.form["txtdescription"]
    availability = request.form.getlist("checkbox")
    st = ""
    for u in availability:
        st = st + u + ","
    c = conn()
    ins = "INSERT INTO ambulance(`Name`,`VehicleNo`,`Place`,Latitude,`Longitude`,`EngineNo`,`phnNo`,`Discription`,`Availability`,hid,email) VALUES('" + name + "','" + vehicleno + "','" + place + "','" + post + "','" + pincode + "','" + Engineno + "','" + phnno + "','" + discription + "','" + st + "','"+hid2+"','"+uname+"')"
    print(ins)
    c.nonreturn(ins)

    ins2 = "insert into userlogin values('"+uname+"','"+pwd+"','ambu')"
    print(ins2)
    c.nonreturn(ins2)

    return render_template("ADMINHOME.html")


@app.route("/h_addambulance_post", methods=['post'])
def haddambulance_post():
    hid = session["hid"]
    name = request.form['txtname']
    vehicleno = request.form["txtvehicleno"]
    place = request.form["Place"]
    post = request.form["Post"]
    pincode = request.form["Pincode"]
    Engineno = request.form["txtengineno"]
    phnno = request.form["txtphno"]
    discription = request.form["txtdescription"]
    availability = request.form.getlist("checkbox")
    st = ""
    for u in availability:
        st = st + u + ","
    c = conn()
    ins = "INSERT INTO ambulance(Name,VehicleNo,Place,Latitude,Longitude,EngineNo,phnNo,Discription,Availability,hid) VALUES('" + name + "','" + vehicleno + "','" + place + "','" + post + "','" + pincode + "','" + Engineno + "','" + phnno + "','" + discription + "','" + st + "','" + str(
        hid) + "')"
    print(ins)
    c.nonreturn(ins)
    return h_addambulance()


@app.route("/ambulanceview")
def addambulanceview():
    c = conn()
    sel = "select * from ambulance"
    print(sel)
    view = c.selectall(sel)
    print(view)
    return render_template("admin_view ambulance.html", res=view)


@app.route("/Labview")
def alablanceview():
    c = conn()
    sel = "select * from lab1"
    print(sel)
    view = c.selectall(sel)
    print(view)
    return render_template("admin_labview.html", res=view)


@app.route("/Pharmacyeview")
def Pharmacyeview():
    c = conn()
    sel = "select pid,addhospital.name,phar.cno,phar.email from phar,addhospital where addhospital.hid=phar.hid"
    print(sel)
    view = c.selectall(sel)
    print(view)
    return render_template("admin_view_phar.html", res=view)

@app.route("/phar_edit/<aid>")
def phar_edit(aid):
    c = conn()
    session["mrr"]=aid
    sel = "select * from phar where pid='" + aid + "'"
    view = c.selectone(sel)

    return render_template("admin_edit_pharma.html", res=view)



@app.route("/lab_edit/<aid>")
def lab_edit(aid):
    c = conn()
    session["mrr"]=aid
    sel = "select * from lab1 where pid='" + aid + "'"
    view = c.selectone(sel)

    return render_template("admin_edit_lab.html", res=view)



@app.route('/lab_delete/<bid>')
def lab_delete(bid):
    c = conn()
    dlt = "delete from lab1 where pid='" + str(bid) + "'"
    res = c.nonreturn(dlt)
    return addambulanceview()

@app.route('/phar_delete/<bid>')
def phr_delete(bid):
    c = conn()
    dlt = "delete from phar where pid='" + str(bid) + "'"
    res = c.nonreturn(dlt)
    return addambulanceview()



@app.route("/updtphar_post", methods=['post'])
def updatepharce():
    hid=request.form["sss"]
    cno=request.form["txt_cno"]

    c = conn()
    upd = "update phar set hid='"+hid+"',cno='"+cno+"' where pid='"+session["mrr"]+"'"
    res = c.nonreturn(upd)
    return render_template("ADMINHOME.html")



@app.route("/updtlab_post", methods=['post'])
def updatelabrce():
    hid=request.form["sss"]
    cno=request.form["txt_cno"]

    c = conn()
    upd = "update lab1 set hid='"+hid+"',cno='"+cno+"' where pid='"+session["mrr"]+"'"
    res = c.nonreturn(upd)
    return render_template("ADMINHOME.html")

@app.route("/ambulance_edit/<aid>")
def ambulance_edit(aid):
    c = conn()
    sel = "select * from ambulance where id='" + aid + "'"
    view = c.selectone(sel)

    return render_template("admineditambulance.html", res=view)


@app.route("/updtambulance_post", methods=['post'])
def updateambulance():
    id = request.form['id']
    name = request.form['txtname']
    vehicleno = request.form["txtvehicleno"]
    place = request.form["Place"]
    post = request.form["Post"]
    pincode = request.form["Pincode"]
    Engineno = request.form["txtengineno"]
    phnno = request.form["txtphno"]
    discription = request.form["txtdescription"]
    availability = request.form.getlist("checkbox")
    st = ""
    for u in availability:
        st = st + u + ","

    c = conn()
    upd = "update ambulance set Name='" + name + "',VehicleNo='" + vehicleno + "',Place='" + place + "',Latitude='" + post + "',Longitude='" + pincode + "',EngineNo='" + Engineno + "',phnNo='" + phnno + "',Discription='" + discription + "',Availability='" + st + "' where id='" + str(
        id) + "'"
    res = c.nonreturn(upd)
    return addambulanceview()


@app.route('/ambulance_delete/<bid>')
def delete(bid):
    c = conn()
    dlt = "delete from ambulance where id='" + str(bid) + "'"
    res = c.nonreturn(dlt)
    return addambulanceview()


@app.route("/usrfd")
def usrfd():
    s = "select feedback.uid, patient_entry.name,feedback.feedback,feedback.date from feedback inner join patient_entry on feedback.uid=patient_entry.Pid"
    c = conn()
    res = c.selectall(s)
    return render_template("admin_user feedback.html", usr=res)


@app.route("/adudetails")
def ad_udetails():
    ud = "select * from patient_entry"
    c = conn()
    res = c.selectall(ud)
    return render_template("admin_view user details.html", aud=res)


@app.route("/usearch", methods=["post"])
def usearch():
    s = request.form["txtname"]
    us = "select * from patient_entry WHERE name like '" + s + "%'"
    c = conn()
    res = c.selectall(us)
    return render_template("admin_view user details.html", aud=res)


@app.route("/addhospital", methods=["post"])
def addhospital():
    c = conn()
    name = request.form["txtname"]
    # state = request.form["state"]
    district = request.form["txtdistrict"]
    pincode = request.form["txtpincode"]
    phno = request.form["txtphno"]
    email = request.form["txtemail"]
    website = request.form["txtwebsite"]
    discription = request.form["txtdescription"]
    image1 = request.files["img1"]
    image2 = request.files["img2"]
    s = "select max(Hid) from addhospital"
    r = c.mid(s)
    image1.save("D:\\mhealth\\mhealth\\\static\\img\\img1-" + str(r) + ".jpg")
    path1 = "/static/img/img1-" + str(r) + ".jpg"
    image2.save("D:\\mhealth\\mhealth\static\\img\\img2-" + str(r) + ".jpg")
    path2 = "/static/img/img2-" + str(r) + ".jpg"
    availability = request.form.getlist("chlab")

    st = ""
    for u in availability:
        st = st + u + ","
    c = conn()
    ah = "insert into addhospital VALUES ('" + str(
        r) + "','" + name + "','kerala','" + district + "','" + pincode + "','" + phno + "','" + email + "','" + website + "','" + path1 + "','" + path2 + "','" + discription + "','" + st + "')"
    res = c.nonreturn(ah)

    ul = "insert into userlogin values('" + email + "','" + phno + "','hospital')"
    c.nonreturn(ul)

    return render_template("ADMINHOME.html")


@app.route("/ahospital")
def ahospital():
    return render_template("adminhospital.html")


@app.route("/adviewhospital")
def adviewhospital():
    c = conn()
    sel = "select * from addhospital"
    view = c.selectall(sel)

    return render_template("admin_view hospital.html", res=view)


@app.route("/hospital_edit/<aid>")
def hospital_edit(aid):
    c = conn()
    sel = "select * from addhospital where hid='" + aid + "'"
    view = c.selectone(sel)

    return render_template("admin_edit_hospital.html", res=view)


@app.route("/updthospital_post", methods=['post'])
def updatehospital():
    hid = request.form['id']
    name = request.form['txtname']
    # state = request.form["state"]
    district = request.form["txtdistrict"]
    pincode = request.form["txtpincode"]
    phnno = request.form["txtphno"]
    email = request.form["txtemail"]
    website = request.form["txtwebsite"]

    discription = request.form["txtdescription"]
    availability = request.form.getlist("chlab")
    c = conn()

    st = ""
    for u in availability:
        st = st + u + ","
    if "img1" in request.files and "img2" in request.files:
        image1 = request.files["img1"]
        image2 = request.files["img2"]
        image1.save("D:\\mhealth\\mhealth\\static\\img\\img1-" + hid + ".jpg")
        path1 = "/static/img/img1-" + hid + ".jpg"
        image2.save("D:\\mhealth\\mhealth\\static\\img\\img2-" + hid + ".jpg")
        path2 = "/static/img/img2-" + hid + ".jpg"
        upd = "update addhospital set Name='" + name + "',district='" + district + "',pincode='" + pincode + "',Phno='" + phnno + "',email='" + email + "',website='" + website + "',image1='" + path1 + "',image2='" + path2 + "',Discription='" + discription + "',Availability='" + st + "' where hid='" + str(
            hid) + "'"
    else:
        upd = "update addhospital set Name='" + name + "',district='" + district + "',pincode='" + pincode + "',Phno='" + phnno + "',email='" + email + "',website='" + website + "',Discription='" + discription + "',Availability='" + st + "' where hid='" + str(
            hid) + "'"
    res = c.nonreturn(upd)
    return render_template("ADMINHOME.html")


@app.route('/hospital_delete/<bid>')
def deletehospital(bid):
    c = conn()
    dlt = "delete from addhospital where hid='" + str(bid) + "'"
    res = c.nonreturn(dlt)
    return "ok"


@app.route('/viewcomp')
def viewcomp():
    s = "select complainthos.cid,complainthos.complaint,complainthos.date,complainthos.status,complainthos.reply,patient_entry.name,addhospital.name,patient_entry.pid,patient_entry.email from patient_entry inner join complainthos on complainthos.uid=patient_entry.Pid inner join addhospital on addhospital.hid=patient_entry.Hid"
    c = conn()
    print(s)
    view = c.selectall(s)
    print(view)
    return render_template("/admin_view hospital complaint.html", res=view)

@app.route('/ad')
def ad():
    return render_template('hsd.html')
@app.route('/lg')
def loginuser():
    return render_template("userlogin.html")


@app.route('/ulogin', methods=['post'])
def ulogin():
    print("jjj")
    username = request.form['username']
    password = request.form['password']
    g = "select * from userlogin WHERE username='" + username + "' AND password='" + password + "'"
    print(g)
    c = conn()
    print(g)
    res = c.selectone(g)
    if res is not None:
        if res[2] == "admin":
            return render_template("ADMINHOME.html")
        elif res[2] == "hospital":

            W = "select hid from addhospital WHERE email='" + username + "'"
            print(W)
            res = c.selectone(W)
            if res is not None:
                print("helllo")
                session["hid"] = res[0]

                return render_template("hospitalhome.html")
            else:
                print("hiii")
                return render_template("index.html")

        else:
            return render_template("index.html")
    else:
        return render_template("index.html")


@app.route('/h_adddoctor')
def h_adddoct():
    v = "select * from department WHERE hid='" + str(session["hid"]) + "'"

    c = conn()
    res = c.selectall(v)
    return render_template("hospital_add doctor.html", res=res)


@app.route('/h_adddoct', methods=['post'])
def h_addoct():
    c = conn()
    name = request.form["txtname"]
    age = request.form["txtage"]
    place = request.form["txtplace"]
    department = request.form["list"]
    qualification = request.form["txtqualification"]
    email = request.form["txtemail"]
    image = request.files["fileimg"]
    phno = request.form["txtphno"]

    address = request.form["txtaddress"]
    s = "select max(did) from doctor"
    r = c.mid(s)
    image.save("D:\\mhealth\\mhealth\static\\doctimg\\img1-" + str(r) + ".jpg")
    path = "/static/doctimg/img1-" + str(r) + ".jpg"
    a = "insert into doctor (hid,name,age,place,department,qualification,email,image,phno,address) values ('" + str(
        session[
            "hid"]) + "','" + name + "','" + age + "','" + place + "','" + department + "','" + qualification + "','" + email + "','" + path + "','" + phno + "','" + address + "')"
    print(a)
    res = c.nonreturn(a)

    yy = random.randint(00000, 99999)

    bb="insert into userlogin values('"+email+"','"+str(yy)+"','doct') "
    c.nonreturn(bb)

    return h_adddoct()


@app.route('/h_viewdoctor')
def h_viewdoct():
    c = conn()
    f = "select * from doctor"
    view = c.selectall(f)
    return render_template("hospital_view doctor.html", res=view)


@app.route('/h_adddep')
def h_adddep():
    return render_template("hospital_add depart.html")


@app.route('/h_dep', methods=['post'])
def h_dep():
    c = conn()
    depname = request.form["txtdepartment"]
    description = request.form["txtdescription"]

    b = "insert into department (hid,depname,description) values ('" + str(
        session["hid"]) + "','" + depname + "','" + description + "')"
    c.nonreturn(b)
    return h_adddep()


@app.route('/h_viewdep')
def h_viewdep():
    c = conn()
    sel = "select * from department WHERE hid='" + str(session["hid"]) + "'"
    view = c.selectall(sel)
    return render_template("hospital_viewdepartment.html", res=view)


@app.route('/hviewdep_delete/<depid>')
def deletedep(depid):
    c = conn()
    dlt = "delete from department where depid='" + str(depid) + "'"
    res = c.nonreturn(dlt)
    return h_viewdep()


@app.route('/hviewdoc_delete/<did>')
def delete_doctor(did):
    c = conn()
    dlt = "delete from doctor where did='" + str(did) + "'"
    res = c.nonreturn(dlt)
    return h_viewdoct()


@app.route('/h_doctoredit/<did>')
def doctor_edit(did):
    c = conn()
    sel = "select * from doctor where did='" + did + "'"
    view = c.selectone(sel)
    sel1 = "select * from department "
    res1 = c.selectall(sel1)
    return render_template('update_hdoctor.html', res=view, res1=res1)


@app.route("/update_doc", methods=['POST'])
def update_doc():
    c = conn()
    did = request.form["did"]
    name = request.form["txtname"]
    age = request.form["txtage"]
    place = request.form["txtplace"]
    department = request.form["list"]
    qualification = request.form["txtqualification"]
    email = request.form["txtemail"]
    phno = request.form["txtphno"]

    address = request.form["txtaddress"]
    if "fileimg" in request.files:
        image = request.files["fileimg"]

        image.save("D:\\mhealth\\mhealth\static\\doctimg\\img1-" + did + ".jpg")
        path = "/static/doctimg/img1-" + did + ".jpg"
        upd = "update doctor set name='" + name + "',age='" + age + "',place='" + place + "',department='" + department + "',qualification='" + qualification + "',email='" + email + "',image='" + path + "',phno='" + phno + "',address='" + address + "' where did='" + str(
            did) + "'"
    else:
        upd = "update doctor set name='" + name + "',age='" + age + "',place='" + place + "',department='" + department + "',qualification='" + qualification + "',email='" + email + "',phno='" + phno + "',address='" + address + "' where did='" + str(
            did) + "'"
    c.nonreturn(upd)
    return h_viewdoct()


@app.route("/dep_edit/<depid>")
def dep_edit(depid):
    c = conn()
    sel = "select * from department where depid='" + depid + "'"
    view = c.selectone(sel)
    return render_template('update_department.html', res=view)


@app.route("/update_dep", methods=['POST'])
def update_dep():
    depname = request.form['txtdepartment']
    description = request.form['txtdescription']
    depid = request.form['id']
    c = conn()
    upd = "update department set depname='" + depname + "',Description='" + description + "' where depid='" + str(
        depid) + "'"
    res = c.nonreturn(upd)
    return h_viewdep()


@app.route('/hpentry')
def hpentry():
    return render_template("hospital_patiententry.html")


@app.route('/h_pentry', methods=['post'])
def h_pentry():
    c = conn()

    name = request.form["txtname"]
    age = request.form["txtage"]
    gender = request.form["txtgender"]
    bgroup = request.form["txtbgroup"]
    dob = request.form["txtdob"]
    place = request.form["txtplace"]
    phno = request.form["txtphno"]
    pdisease = request.form["txtpreviousdisease"]
    cdisease = request.form["txtcurrentdisease"]
    email = request.form["txtemail"]
    #####
    image = request.files["fileimg"]
    s = "select max(Pid) from patient_entry"
    r = c.mid(s)
    image.save("D:\\mhealth\\mhealth\static\\pat_img\\img1-" + str(r) + ".jpg")
    path = "/static/pat_img/img1-" + str(r) + ".jpg"

    ###



    d = "insert into patient_entry(hid,name,age,gender,bloodgroup,DOB,place,phno,pdisease,disease,email,photo)values('" + str(
        session[
            "hid"]) + "','" + name + "','" + age + "','" + gender + "','" + bgroup + "','" + dob + "','" + place + "','" + phno + "','" + pdisease + "','" + cdisease + "','" + email + "','"+path+"')"
    c.nonreturn(d)
    yy = random.randint(00000, 99999)

    qq="insert into userlogin values('"+email+"','"+str(yy)+"','pati')"
    c.nonreturn(qq)

    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
    )
    qr.add_data(str(r))
    qr.make(fit=True)

    img = qr.make_image(fill_color="black", back_color="white")
    img.save("D:\\mhealth\\mhealth\static\\qq\\"+str(r)+".jpg")

    ###MAIL
    # s1 = smtplib.SMTP(host='smtp.gmail.com', port=587)
    # s1.starttls()
    # s1.login("riss.staff@gmail.com", "rissstaff")
    # msg = MIMEMultipart()  # create a message
    # message = "your password...................."
    # print(message)
    # msg['From'] = "riss.likhil@gmail.com"
    # msg['To'] = emailid
    # msg['Subject'] = "YOUR PASSWORD"
    # em = "jj"
    # rsndm = yy
    # body = "Password=" + str(rsndm)
    # msg.attach(MIMEText(str(body), 'plain'))
    # s1.send_message(msg)


    ###

    return hpentry()


@app.route('/h_viewpatient')
def h_viewpatient():
    c = conn()
    sel = "select * from patient_entry WHERE hid='" + str(session["hid"]) + "'"
    view = c.selectall(sel)
    return render_template("hospital_patientview.html", res=view)


@app.route('/pedit/<pid>')
def pedit(pid):
    c = conn()
    sel = "select * from patient_entry WHERE pid='" + pid + "'"
    view = c.selectone(sel)
    return render_template('updatepatient.html', res=view)


@app.route('/updpatient', methods=['post'])
def updpatient():
    pid = request.form["id"]
    name = request.form["txtname"]
    age = request.form["txtage"]
    gender = request.form["txtgender"]
    bgroup = request.form["txtbgroup"]
    dob = request.form["txtdob"]
    place = request.form["txtplace"]
    phno = request.form["txtphno"]
    pdisease = request.form["txtpreviousdisease"]
    cdisease = request.form["txtcurrentdisease"]
    email = request.form["txtemail"]
    c = conn()

    g = "update  patient_entry set hid='" + str(session[
                                                    "hid"]) + "',name='" + name + "',age='" + age + "',gender='" + gender + "',bloodgroup='" + bgroup + "',dob='" + dob + "',place='" + place + "',phno='" + phno + "',pdisease='" + pdisease + "',disease='" + cdisease + "',email='" + email + "' where pid='" + str(
        pid) + "'"
    c.nonreturn(g)
    return h_viewpatient()


@app.route('/pdel/<pid>')
def pdelete(pid):
    c = conn()
    dlt = "delete from patient_entry WHERE pid='" + str(pid) + "'"
    res = c.nonreturn(dlt)
    return h_viewpatient()


@app.route("/h_ambulanceview")
def hadambulanceview():
    c = conn()
    sel = "select * from ambulance"
    print(sel)
    view = c.selectall(sel)
    print(view)
    return render_template("hospital_view ambulance.html", res=view)


@app.route('/h_doctorsh')
def h_doctors():
    c = conn()
    a = "select * from doctor"
    res = c.selectall(a)

    b = "select * from department"
    res1 = c.selectall(b)

    return render_template("hospital_doctor sheduling.html", res=res, res1=res1)


@app.route('/h_doctsh', methods=['post'])
def h_doctsh():
    date = request.form["txtdate"]
    frm = request.form["txtfrom"]
    to = request.form["txtto"]
    did = request.form["listdoctor"]
    c = conn()

    d = "insert into doctorsh (did,tdate,tfrom,tto) VALUES ('" + did + "','" + date + "','" + frm + "','" + to + "')"
    c.nonreturn(d)
    return h_doctors()


@app.route('/h_shview')
def h_shview():
    a = "select department.depname,doctor.name,doctor.phno,doctorsh.tdate,doctorsh.tfrom,doctorsh.tto,doctorsh.sid from department,doctor,doctorsh where department.depid=doctor.department and doctor.did=doctorsh.did"
    c = conn()
    view = c.selectall(a)

    return render_template("hospital_view sheduling.html", res=view)


@app.route('/shdel/<sid>')
def shdel(sid):
    c = conn()
    dlt = "delete from doctorsh WHERE sid='" + str(sid) + "' "
    print(dlt)
    res = c.nonreturn(dlt)
    return h_shview()


@app.route('/h_addambulance')
def h_addambulance():
    return render_template("hospital_addambulance.html")


@app.route('/h_viewcomp')
def h_viewreplay():
    a = "select * from complainthos where hid='" + str(session["hid"]) + "'"
    c = conn()
    res = c.selectall(a)
    return render_template("hospital_view replay.html", res=res)


@app.route('/viewreplay/<cid>')
def viewreplay(cid):
    c = conn()
    sel = "select complaint,reply from complainthos WHERE cid='" + str(cid) + "' "
    res = c.selectone(sel)
    return render_template("hrply.html", res=res)


@app.route('/loginpage')
def loginpage():
    return render_template("index.html")


@app.route('/h_sendcomp')
def h_sendcomp():
    return render_template("hospital_send complaint.html")


@app.route('/h_sndcomp', methods=['post'])
def h_sndcomp():
    comp = request.form["txtcomplaint"]
    a = "insert into complainthos (complaint,hid,date,type) values ('" + comp + "','" + str(
        session["hid"]) + "',curdate(),'hospital') "
    c = conn()
    res = c.nonreturn(a)
    return "ok"


@app.route('/h_viewbooking')
def h_viewbooking():
    a = "select doctor.name,department.depname,doctorbooking.date,patient_entry.name,patient_entry.phno,patient_entry.place,patient_entry.email from doctor ,department,doctorbooking,patient_entry where doctor.department=department.depid and doctorbooking.did=doctor.did and doctorbooking.uid=patient_entry.Pid and doctor.hid=3"

    c = conn()
    res = c.selectall(a)
    return render_template("hospital_view booking.html", res=res)


@app.route('/h_ambulanceview')
def h_viewambulance():
    return render_template("hospital_view ambulance.html")


@app.route('/admin')
def admin():
    return render_template("admintemp.html")


@app.route('/ahome')
def ahome():
    return render_template("ADMINHOME.html")


@app.route('/hospital')
def hos():
    return render_template("hospitaltemp.html")


@app.route('/hoshome')
def hoshome():
    return render_template("hospitalhome.html")


if __name__ == "__main__":
    app.run(debug=True)
