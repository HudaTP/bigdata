/*
SQLyog Community Edition- MySQL GUI v8.03 
MySQL - 5.6.12-log : Database - mhlth
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`mhlth` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mhlth`;

/*Table structure for table `addhospital` */

DROP TABLE IF EXISTS `addhospital`;

CREATE TABLE `addhospital` (
  `hid` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `district` varchar(30) DEFAULT NULL,
  `pincode` decimal(12,0) DEFAULT NULL,
  `phno` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `website` varchar(30) DEFAULT NULL,
  `image1` varchar(100) DEFAULT NULL,
  `image2` varchar(100) DEFAULT NULL,
  `discription` varchar(40) DEFAULT NULL,
  `availability` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`hid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `addhospital` */

insert  into `addhospital`(`hid`,`name`,`state`,`district`,`pincode`,`phno`,`email`,`website`,`image1`,`image2`,`discription`,`availability`) values (1,'MALABAR','KERALA','KOZHIKODE','653771','9562155900','malabar1@gmail.com','www.malabar.com','/static/img/img1-1.jpg','/static/img/img2-1.jpg','good hospital',''),(2,'MALABAR','KERALA','KOZHIKODE','653771','9562155900','malabar1@gmail.com','www.malabar.com','/static/img/img1-2.jpg','/static/img/img2-2.jpg','good hospital',''),(3,'BMH','KERALA','KOZHIKODE','653771','9999999999','BMH@GMAIL.COM','WWW.BMH.COM','/static/img/img1-3.jpg','/static/img/img2-3.jpg','BMH','Lab,amulance,pharmacy,Blood bank,'),(4,'abhi','KERALA','KOZHIKODE','653771','9895395848','abhi@gmail.com','WWW.BMH.COM','/static/img/img1-4.jpg','/static/img/img2-4.jpg','abi','Lab,pharmacy,Blood bank,'),(5,'abhi','KERALA','KOZHIKODE','653771','0495280023','abhi@gmail.com','WWW.BMH.COM','/static/img/img1-5.jpg','/static/img/img2-5.jpg','abi','amulance,pharmacy,'),(6,'kims','KERALA','KOZHIKODE','673571','232352','kims@gmail.com','www.kims.com','/static/img/img1-6.jpg','/static/img/img2-6.jpg','kims vennakad','Lab,amulance,pharmacy,Blood bank,'),(7,'MALABAR','KERALA','KOZHIKODE','653771','9562155900','malabar1@gmail.com','www.malabar.com','/static/img/img1-7.jpg','/static/img/img2-7.jpg','good hospital','Lab,amulance,pharmacy,Blood bank,Organ bank,');

/*Table structure for table `ambulance` */

DROP TABLE IF EXISTS `ambulance`;

CREATE TABLE `ambulance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) DEFAULT NULL,
  `VehicleNo` varchar(50) NOT NULL,
  `Place` varchar(30) DEFAULT NULL,
  `Latitude` varchar(30) DEFAULT NULL,
  `Longitude` varchar(30) DEFAULT NULL,
  `EngineNo` varchar(40) DEFAULT NULL,
  `PhnNo` bigint(12) DEFAULT NULL,
  `Discription` varchar(100) DEFAULT NULL,
  `Availability` varchar(100) DEFAULT NULL,
  `hid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `ambulance` */

insert  into `ambulance`(`id`,`Name`,`VehicleNo`,`Place`,`Latitude`,`Longitude`,`EngineNo`,`PhnNo`,`Discription`,`Availability`,`hid`) values (1,'devi','kl 57 e 07','kgm','kgm','5362376','ijh97895kn',98635241,'ambulance','on,',4),(2,'BMH','kl 57 e 07','mavoor','mavoor','5362376','ijh97895kn',98635241,'ambulance','Oxygen,',4),(3,'ederwefr3','ijjih','ijh','ioh','+665','kjhjkb',984878974,'kjijgivb','Freezer,',4),(6,'kims','KL 57 9895','vennakad','padanilam','673571','ghgh878455njhv',9895623141,'kims ambulance','Oxygen,',0);

/*Table structure for table `complainthos` */

DROP TABLE IF EXISTS `complainthos`;

CREATE TABLE `complainthos` (
  `cid` int(5) NOT NULL AUTO_INCREMENT,
  `complaint` varchar(250) DEFAULT NULL,
  `hid` int(10) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `reply` varchar(250) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `complainthos` */

insert  into `complainthos`(`cid`,`complaint`,`hid`,`status`,`reply`,`date`,`type`,`uid`) values (1,'very bad',4,NULL,NULL,'2019-03-05','hospital',NULL),(2,'iugug',1,'',NULL,'2019-05-01','patient',1);

/*Table structure for table `department` */

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `depid` int(11) NOT NULL AUTO_INCREMENT,
  `hid` int(11) DEFAULT NULL,
  `depname` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`depid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `department` */

insert  into `department`(`depid`,`hid`,`depname`,`description`) values (1,4,'card','heart dep');

/*Table structure for table `doctor` */

DROP TABLE IF EXISTS `doctor`;

CREATE TABLE `doctor` (
  `did` int(11) NOT NULL AUTO_INCREMENT,
  `hid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `place` varchar(50) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `qualification` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `phno` int(11) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `doctor` */

insert  into `doctor`(`did`,`hid`,`name`,`age`,`place`,`department`,`qualification`,`email`,`image`,`phno`,`address`) values (2,4,'pavithran',28,'kozhikode','1','mbbs,md','pavi@gmail.com','/static/doctimg/img1-2.jpg',495100788,'pavilayam(h)pavangad');

/*Table structure for table `doctorbooking` */

DROP TABLE IF EXISTS `doctorbooking`;

CREATE TABLE `doctorbooking` (
  `bid` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `hid` int(11) DEFAULT NULL,
  `dname` varchar(30) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `phno` int(11) DEFAULT NULL,
  `place` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`bid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `doctorbooking` */

/*Table structure for table `doctorsh` */

DROP TABLE IF EXISTS `doctorsh`;

CREATE TABLE `doctorsh` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) DEFAULT NULL,
  `tdate` date DEFAULT NULL,
  `tfrom` varchar(50) DEFAULT NULL,
  `tto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `doctorsh` */

insert  into `doctorsh`(`sid`,`did`,`tdate`,`tfrom`,`tto`) values (1,2,'0000-00-00','10 am','4 pm'),(2,2,'2019-03-24','10 am','4 pm');

/*Table structure for table `feedback` */

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `uid` int(10) NOT NULL,
  `fid` int(10) NOT NULL AUTO_INCREMENT,
  `feedback` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `feedback` */

/*Table structure for table `patient_entry` */

DROP TABLE IF EXISTS `patient_entry`;

CREATE TABLE `patient_entry` (
  `Pid` int(11) NOT NULL AUTO_INCREMENT,
  `Hid` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `age` varchar(10) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `bloodgroup` varchar(10) DEFAULT NULL,
  `DOB` varchar(10) DEFAULT NULL,
  `place` varchar(20) DEFAULT NULL,
  `phno` varchar(10) DEFAULT NULL,
  `pdisease` varchar(40) DEFAULT NULL,
  `disease` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Pid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `patient_entry` */

insert  into `patient_entry`(`Pid`,`Hid`,`name`,`age`,`gender`,`bloodgroup`,`DOB`,`place`,`phno`,`pdisease`,`disease`,`email`) values (1,1,'soman','12','male','b','1998-12-23','khm','5585415','hgfyg','hbhgc','gffdfd9+fdf');

/*Table structure for table `userlogin` */

DROP TABLE IF EXISTS `userlogin`;

CREATE TABLE `userlogin` (
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `userlogin` */

insert  into `userlogin`(`username`,`password`,`type`) values ('ADMIN','ADMIN','admin'),('abhi@gmail.com','9895395848','hospital'),('abhi@gmail.com','9895395848','hospital'),('kims@gmail.com','232352','hospital'),('malabar1@gmail.com','9562155900','hospital');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
