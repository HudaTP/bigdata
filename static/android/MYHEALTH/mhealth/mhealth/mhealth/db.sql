/*
SQLyog Community v13.1.0 (32 bit)
MySQL - 5.5.20-log : Database - mhlth
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mhlth` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mhlth`;

/*Table structure for table `ambulance` */

DROP TABLE IF EXISTS `ambulance`;

CREATE TABLE `ambulance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) DEFAULT NULL,
  `VehicleNo` varchar(10) NOT NULL,
  `Place` varchar(30) DEFAULT NULL,
  `Post` varchar(30) DEFAULT NULL,
  `Pincode` varchar(10) DEFAULT NULL,
  `EngineNo` varchar(20) DEFAULT NULL,
  `phnNo` bigint(12) DEFAULT NULL,
  `Discription` varchar(100) DEFAULT NULL,
  `Availability` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `ambulance` */

insert  into `ambulance`(`id`,`Name`,`VehicleNo`,`Place`,`Post`,`Pincode`,`EngineNo`,`phnNo`,`Discription`,`Availability`) values 
(1,'gbgd','4355454','fddhg','fddgdhgd','353533','ggh4rtt',4444464,'hgfgffjfhfhf','on,on,'),
(2,'athira','kl 57 h 98','thamarassery','perumpally','673586','enghnd3232345in',7907315256,'good vehicle\r\nhigh light system\r\npushback seat\r\nvarma sounds','Mobile ICU,Oxygen,Freezer,');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
