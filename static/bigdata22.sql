/*
SQLyog Community Edition- MySQL GUI v8.03 
MySQL - 5.6.12-log : Database - mhlth
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`mhlth` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mhlth`;

/*Table structure for table `addhospital` */

DROP TABLE IF EXISTS `addhospital`;

CREATE TABLE `addhospital` (
  `hid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `district` varchar(30) DEFAULT NULL,
  `pincode` decimal(12,0) DEFAULT NULL,
  `phno` varchar(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `website` varchar(30) DEFAULT NULL,
  `image1` varchar(100) DEFAULT NULL,
  `image2` varchar(100) DEFAULT NULL,
  `discription` varchar(40) DEFAULT NULL,
  `availability` varchar(50) DEFAULT NULL,
  `lid` int(11) DEFAULT NULL,
  `lati` varchar(50) DEFAULT NULL,
  `longi` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`hid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `addhospital` */

insert  into `addhospital`(`hid`,`name`,`state`,`district`,`pincode`,`phno`,`email`,`website`,`image1`,`image2`,`discription`,`availability`,`lid`,`lati`,`longi`) values (4,'mims','kerala','Calicut','673355','9876543212','mims@gmail.com','www.mims.com','img/img1-4.jpg','img/img2-4.jpg','descr','Blood bank,Organ bank,',2,'11.25','75.78'),(5,'kg','kerala','Kannur','673344','9876543212','kg@g.com','www.kg.com','img/img1-3.jpg','img/img2-3.jpg','descr','Blood bank,',3,'11.65','75.75'),(6,'pkm','kerala','Wayanad','678899','9876543212','pkm@g.com','pkm.com','img/img1-4.jpg','img/img2-4.jpg','descri','Organ bank,',4,'11.17','75.86');

/*Table structure for table `ans` */

DROP TABLE IF EXISTS `ans`;

CREATE TABLE `ans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `an1` varchar(100) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  `an2` varchar(100) DEFAULT NULL,
  `an3` varchar(100) DEFAULT NULL,
  `an4` varchar(100) DEFAULT NULL,
  `an5` varchar(100) DEFAULT NULL,
  `an6` varchar(100) DEFAULT NULL,
  `an7` varchar(100) DEFAULT NULL,
  `an8` varchar(100) DEFAULT NULL,
  `an9` varchar(100) DEFAULT NULL,
  `an10` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `ans` */

insert  into `ans`(`id`,`an1`,`did`,`an2`,`an3`,`an4`,`an5`,`an6`,`an7`,`an8`,`an9`,`an10`) values (6,'',0,'','','','','','','','',''),(7,'b',7,'b','b','b','b','b','b','b','b','b'),(8,'a',8,'b','c','d','e','f','g','h','i','j'),(9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `ans2` */

DROP TABLE IF EXISTS `ans2`;

CREATE TABLE `ans2` (
  `qid` int(11) DEFAULT NULL,
  `qst` varchar(100) DEFAULT NULL,
  `an1` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ans2` */

/*Table structure for table `bank` */

DROP TABLE IF EXISTS `bank`;

CREATE TABLE `bank` (
  `ac` varchar(50) DEFAULT NULL,
  `pwd` varchar(50) DEFAULT NULL,
  `amt` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bank` */

/*Table structure for table `complaint` */

DROP TABLE IF EXISTS `complaint`;

CREATE TABLE `complaint` (
  `complaint_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(55) DEFAULT NULL,
  `complaint` varchar(300) DEFAULT NULL,
  `date` varchar(50) DEFAULT NULL,
  `reply` varchar(50) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`complaint_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `complaint` */

/*Table structure for table `department` */

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `depid` int(11) NOT NULL AUTO_INCREMENT,
  `hid` int(11) DEFAULT NULL,
  `depname` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`depid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `department` */

insert  into `department`(`depid`,`hid`,`depname`,`description`) values (5,4,'ortho','dee'),(6,4,'nuro','dd');

/*Table structure for table `doctor` */

DROP TABLE IF EXISTS `doctor`;

CREATE TABLE `doctor` (
  `did` int(11) NOT NULL AUTO_INCREMENT,
  `hid` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `place` varchar(50) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `qualification` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `phno` varchar(12) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `gen` varchar(50) DEFAULT NULL,
  `lid` int(11) DEFAULT NULL,
  PRIMARY KEY (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `doctor` */

insert  into `doctor`(`did`,`hid`,`name`,`age`,`place`,`department`,`qualification`,`email`,`image`,`phno`,`address`,`gen`,`lid`) values (7,4,'manju',22,'plc',5,'qul','ma@g.com','doctimg/img1-7.jpg','9876543212','adrs','Female',7),(8,4,'anu',22,'',5,'qul','anu@g.com','doctimg/img1-10.jpg','9876545678','adr','Female',10);

/*Table structure for table `doctorbooking` */

DROP TABLE IF EXISTS `doctorbooking`;

CREATE TABLE `doctorbooking` (
  `bid` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(50) DEFAULT 'pending',
  `sid` int(11) DEFAULT NULL,
  PRIMARY KEY (`bid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `doctorbooking` */

insert  into `doctorbooking`(`bid`,`did`,`uid`,`date`,`status`,`sid`) values (5,8,3,'2021-06-23','approved',4);

/*Table structure for table `doctorsh` */

DROP TABLE IF EXISTS `doctorsh`;

CREATE TABLE `doctorsh` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) DEFAULT NULL,
  `tdate` varchar(50) DEFAULT NULL,
  `tfrom` varchar(50) DEFAULT NULL,
  `tto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `doctorsh` */

insert  into `doctorsh`(`sid`,`did`,`tdate`,`tfrom`,`tto`) values (4,7,'2021-06-24','2','3');

/*Table structure for table `file22` */

DROP TABLE IF EXISTS `file22`;

CREATE TABLE `file22` (
  `fid` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(250) DEFAULT NULL,
  `date` varchar(50) DEFAULT NULL,
  `f_type` varchar(55) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `file22` */

insert  into `file22`(`fid`,`fname`,`date`,`f_type`,`did`) values (4,'krish','2021-06-23','jpg',8);

/*Table structure for table `new_sk` */

DROP TABLE IF EXISTS `new_sk`;

CREATE TABLE `new_sk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) DEFAULT NULL,
  `ky` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `new_sk` */

insert  into `new_sk`(`id`,`did`,`ky`) values (2,6,'1234567887654328'),(3,7,'1234567887654321'),(4,8,'1234567887654325');

/*Table structure for table `patient_entry` */

DROP TABLE IF EXISTS `patient_entry`;

CREATE TABLE `patient_entry` (
  `Pid` int(11) NOT NULL AUTO_INCREMENT,
  `Hid` int(11) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `bloodgroup` varchar(10) DEFAULT NULL,
  `DOB` varchar(10) DEFAULT NULL,
  `place` varchar(20) DEFAULT NULL,
  `phno` varchar(10) DEFAULT NULL,
  `disease` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `photo` varchar(240) DEFAULT NULL,
  `lid` int(11) DEFAULT NULL,
  PRIMARY KEY (`Pid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `patient_entry` */

insert  into `patient_entry`(`Pid`,`Hid`,`name`,`gender`,`bloodgroup`,`DOB`,`place`,`phno`,`disease`,`email`,`photo`,`lid`) values (3,0,'Mridul','female','B+','12/2/2000','plc','9874563214','dirs','riss.mridul@gmail.com','11_40_21.jpg',9);

/*Table structure for table `precription` */

DROP TABLE IF EXISTS `precription`;

CREATE TABLE `precription` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `prsption` varchar(1000) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  `patint_id` int(11) DEFAULT NULL,
  `date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `precription` */

insert  into `precription`(`pid`,`prsption`,`did`,`patint_id`,`date`) values (6,'pre322\r\nslkksd\r\nkslfkldf\r\nlkflksd 4545\r\n24p 23i48\r\n',7,3,'2021-06-23');

/*Table structure for table `qst` */

DROP TABLE IF EXISTS `qst`;

CREATE TABLE `qst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qst` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `qst` */

insert  into `qst`(`id`,`qst`) values (1,'What was the house number and street name you lived in as a child?'),(2,'What were the last four digits of your childhood telephone number?'),(3,'What primary school did you attend?'),(4,'What is your grandmother\'s (on your mother\'s side) maiden name?'),(5,'In what town or city did your parents meet?'),(6,'What time of the day were you born? (hh:mm)'),(7,'What time of the day was your first child born? (hh:mm)'),(8,'In what town or city did you meet your spouse or partner?'),(9,'What are the last five digits of your driver\'s license number?'),(10,'What is your spouse or partner\'s mother\'s maiden name?');

/*Table structure for table `userlogin` */

DROP TABLE IF EXISTS `userlogin`;

CREATE TABLE `userlogin` (
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `lid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`lid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `userlogin` */

insert  into `userlogin`(`username`,`password`,`type`,`lid`) values ('a','a','admin',1),('mims@gmail.com','a','hospital',2),('kg@g.com','3026','hospital',3),('pkm@g.com','7820','hospital',4),('ma@g.com','a','doct',7),('ma@g.com','b','doct_entr',8),('riss.mridul@gmail.com','aa','pati',9),('anu@g.com','a','doct',10),('anu@g.com','b','doct_entr',11),(NULL,NULL,NULL,12);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
